import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BakeryModule } from './bakery/bakery.module';

@Module({
  imports: [
    BakeryModule,
    MongooseModule.forRoot(
      'mongodb://nestjs-username:nestjs-pwd@localhost:27017',
      { dbName: 'default' },
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
