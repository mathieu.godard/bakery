import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';

const validationPipe = new ValidationPipe({
  transform: true,
  whitelist: true,
});

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalPipes(validationPipe);

  const config = new DocumentBuilder()
    .setTitle('Ma 2ième API avec NestJS')
    .setDescription(
      `Bakery project`,
    )
    .setVersion('1.0')
    .setExternalDoc('Lien vers le github', 'http://perdu.com')
    .setContact('Xavier Cassel', 'http://perdu.com', 'xavier.cassel@zenika.com')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  await app.listen(3000);
}
bootstrap();
