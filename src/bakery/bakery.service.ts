import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ApiTags } from '@nestjs/swagger';
import { Model, ObjectId } from 'mongoose';
import { CreateBakeryDto } from './dto/create-bakery.dto';
import { UpdateBakeryDto } from './dto/update-bakery.dto';
import { Bakery, BakeryDocument } from './entities/bakery.entity';

@Injectable()
export class BakeryService {
  constructor(
    @InjectModel(Bakery.name) private bakeryModel: Model<BakeryDocument>,
  ) {}

  /**
   * créer un livre en base
   * @param bakeryDto
   * @returns la boulangerie est ajoutée
   *
   * @throws {HttpException} remonte une erreur 400 si les données envoyées ne sont pas correctes
   */
  create(createBakeryDto: CreateBakeryDto): Promise<Bakery> {
    if (!createBakeryDto) {
      throw new BadRequestException('Donnée incomplète');
    }
    const createBakery = new this.bakeryModel(createBakeryDto);
    return createBakery.save();
  }

  async findAll(): Promise<Bakery[]> {
    const allBakeries = await this.bakeryModel.find();
    if (!allBakeries || allBakeries.length === 0) {
      throw new NotFoundException("Il n'y a pas de Boulangerie");
    }
    return allBakeries;
  }

  async findOne(id: ObjectId): Promise<Bakery> {
    const theBakery = await this.bakeryModel.findOne({ _id: id });
    if (!theBakery) {
      throw new NotFoundException("La boulangerie n'existe pas ");
    }
    return theBakery;
  }

  async update(id: ObjectId, updateBakeryDto: UpdateBakeryDto): Promise<Bakery> {
    const updatedBakery = await this.bakeryModel.findOneAndUpdate(
      { _id: id },
      updateBakeryDto,
      { new: true },
    );
    if (!updateBakeryDto) {
      throw new NotFoundException("La boulangerie n'existe pas ");
    }
    return updatedBakery;
  }

  async delete(id: ObjectId): Promise<Bakery> {
    const deleteBakery = await this.bakeryModel.findOneAndDelete({ _id: id });
    if (!deleteBakery) {
      throw new NotFoundException("La boulangerie n'existe pas ");
    }
    return deleteBakery;
  }
}
