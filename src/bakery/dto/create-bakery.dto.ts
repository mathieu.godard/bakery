import { IsArray, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { Address } from '../entities/bakery.entity';

export class CreateBakeryDto {
  /**
   * Nom de la boulangerie en français
   * @example "Breizh Paris"
   */
  @IsString()
  @IsNotEmpty()
  name: string;
  /**
   * Adresse de la boulagerie en français
   * @example ""rue de Paris", "35000","Rennes"  "
   */
  @IsNotEmpty()
  address: Address;

  /**
   * La liste des produits en français
   * @example "[Croissant, pain au chocolat]"
   */
   @IsArray()
   @IsString({ each: true })
  products: string[];

  /**
   * Les notes livre en français
   * @example [5, 3, 2]
   */
  @IsArray()
  @IsNumber({},{ each: true })
  rating: number[];
}
