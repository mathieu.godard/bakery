import { PartialType } from '@nestjs/mapped-types';
import { Address } from '../entities/bakery.entity';
import { CreateBakeryDto } from './create-bakery.dto';

export class UpdateBakeryDto extends PartialType(CreateBakeryDto) {}
