import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument } from 'mongoose';

export class Address {
  streetName: string;
  zipCode: number;
  city: string;
}

@Schema()
export class Bakery {
  /**
   * Nom de la boulangerie en français
   * @example "Breiz Paris"
   */
  @Prop({ required: true })
  name: string;

  /**
   * Adresse de la boulagerie en français
   * @example ""rue de Paris", "35000","Rennes"  "
   */
  @Prop({ required: true })
  address: Address;

  /**
   * La liste des produits en français
   * @example "[Croissant, pain au chocolat]"
   */
  @Prop({ default: [] })
  products: string[];

  /**
   * Les notes livre en français
   * @example [5, 3, 2]
   */
  @Prop({ default: [] })
  rating: number[];

  constructor(
    name: string,
    address: Address,
    products: string[],
    rating: number[],
  ) {
    this.name = name;
    this.address = address;
    this.products = products;
    this.rating = rating;
  }
}

export type BakeryDocument = HydratedDocument<Bakery>;
export const BakerySchema = SchemaFactory.createForClass(Bakery);
