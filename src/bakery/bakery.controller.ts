import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Injectable,
  PipeTransform,
  BadRequestException,
} from '@nestjs/common';
import { ApiBadRequestResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { ObjectId, Types } from 'mongoose';
import { BakeryService } from './bakery.service';
import { CreateBakeryDto } from './dto/create-bakery.dto';
import { UpdateBakeryDto } from './dto/update-bakery.dto';

@Injectable()
export class ParseObjectIdPipe implements PipeTransform<any, Types.ObjectId> {
  transform(value: any): Types.ObjectId {
    const validObjectId = Types.ObjectId.isValid(value);

    if (!validObjectId) {
      throw new BadRequestException('Invalid ObjectId');
    }

    return value;
  }
}
@ApiTags('bakeries')
@Controller('bakeries')
export class BakeryController {
  constructor(private readonly bakeryService: BakeryService) {}

  @ApiOperation({ summary: "Création d'une boulangerie" })
  @ApiBadRequestResponse()
  @Post()
  create(@Body() createBakeryDto: CreateBakeryDto) {
    return this.bakeryService.create(createBakeryDto);
  }

  @ApiOperation({ summary: "Récupérer toutes les boulangeries" })
  @Get()
  findAll() {
    return this.bakeryService.findAll();
  }

  @ApiOperation({ summary: "Récupéréer une boulangerie" })
  @Get(':id')
  findOne(@Param('id', ParseObjectIdPipe) id: ObjectId) {
    return this.bakeryService.findOne(id);
  }

  @ApiOperation({ summary: "Modifier une boulangerie" })
  @Patch(':id')
  update(
    @Param('id', ParseObjectIdPipe) id: ObjectId,
    @Body() updateBakeryDto: UpdateBakeryDto,
  ) {
    return this.bakeryService.update(id, updateBakeryDto);
  }

  @ApiOperation({ summary: "Supprimer une boulangerie" })
  @Delete(':id')
  remove(@Param('id', ParseObjectIdPipe) id: ObjectId) {
    return this.bakeryService.delete(id);
  }
}
