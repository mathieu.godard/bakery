import { Module } from '@nestjs/common';
import { BakeryService } from './bakery.service';
import { BakeryController } from './bakery.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Bakery, BakerySchema } from './entities/bakery.entity';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Bakery.name, schema: BakerySchema }]),
  ],
  controllers: [BakeryController],
  providers: [BakeryService],
})
export class BakeryModule {}
